from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404

# Create your views here.
from articles.models import Article

def archive(request):
	return render(request, 'archive.html', {"posts": Article.objects.all()})

def get_article(request, article_id):
	try:
		post = Article.objects.get(id=article_id)
		return render(request, 'article.html', {"post": post})
	except Article.DoesNotExist:
		raise Http404

def create_post(request):
	if request.user.is_anonymous:
		raise Http404
	if request.method == "POST":
		# обработать данные формы, если метод POST
		form = {
			'text': request.POST["text"], 'title': request.POST["title"]
		}
		# в словаре form будет храниться информация, введенная пользователем
		if form["text"] and form["title"]:
			try:
				Article.objects.get(title=form['title'])
			except Article.DoesNotExist:
				# если поля заполнены без ошибок
				article = Article.objects.create(text=form["text"], title=form["title"], author=request.user)
				return redirect('get_article', article_id=article.id)
				# перейти на страницу поста
			else:
				form['errors'] = 'An article with the same name exists, choose another one, please!'
				return render(request, 'addform.html', {'form': form})
		else:
 			# если введенные данные некорректны
			form['errors'] = u"Не все поля заполнены"
			return render(request, 'addform.html', {'form': form})
	else:
		# просто вернуть страницу с формой, если метод GET
		return render(request, 'addform.html', {})
